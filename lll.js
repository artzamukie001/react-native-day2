import React from 'react';
import { StyleSheet, Text, View, Button, Alert, TouchableOpacity, TextInput, Image,ScrollView  } from 'react-native';

class App2 extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            username: 'Username',
            password: 'Password'
        };
      }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.row}>
                        <View style={styles.column}>
                            <View style={styles.image}>
                                <Image
                                    style={styles.image}
                                    source={{uri: 'https://scontent-kut2-2.xx.fbcdn.net/v/t1.0-9/38011840_2447134912016263_7498373180617129984_n.jpg?_nc_cat=111&_nc_ht=scontent-kut2-2.xx&oh=514f87320fefb8b7cfdeaf20499cc65f&oe=5CF1A91A'}}
                                />

                            </View>
                        </View>
                    </View>
                </View>

                <ScrollView style={styles.body}>
                    <View style={styles.column}>
                        <Text style={styles.boxText2}>{this.state.username}-{this.state.password} </Text>
                        <View style={styles.box1}>
                            <TextInput style={styles.boxText1}
                            onChangeText={(username) => this.setState({username})}
                            >input username</TextInput>
                        </View>
                        <View style={styles.box1}>
                            <TextInput style={styles.boxText1}
                            onChangeText={(password) => this.setState({password})}
                            >input password</TextInput>
                        </View>
                    </View>
                </ScrollView>

                <View style={styles.footer}>
                    <View style={styles.box2}>
                        <TouchableOpacity>
                            <Text style={styles.boxText2} onPress={() => { Alert.alert('สวยพี่สวย') }}>TouchableOpacity</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#666',
        flex: 1
    },
    header: {
        backgroundColor: '#666',
        flex: 2
    },
    column: {                   //row
        backgroundColor: '#666',
        flexDirection: 'column',
        alignItems: 'center',     //justifyContent
        flex: 1,
    },
    row: {                      //column
        backgroundColor: '#666',
        flexDirection: 'row',
        alignItems: 'center',     //justifyContent
        flex: 1,
    },
    image: {
        backgroundColor: 'white',
        width: 200,
        height: 200,
        borderRadius: 100,
    },
    radiusText: {
        fontSize: 20,
        fontWeight: 'bold',
        padding: 70,
    },
    body: {
        backgroundColor: 'green',
        flex: 1
    },
    box1: {
        backgroundColor: 'white',
        flexDirection: 'column',
        alignItems: 'center',
        width: 300,
        height: 60,
        borderRadius: 10,
        margin: 14,
    },
    box2: {
        backgroundColor: '#330000',
        flexDirection: 'column',
        alignItems: 'center',
        width: 300,
        height: 60,
        borderRadius: 10,
        margin: 14,
    },
    boxText1: {
        fontSize: 20,
        fontWeight: 'bold',
        padding: 10,
    },
    footer: {
        backgroundColor: '#666',
        alignItems: 'center',
        justifyContent: 'flex-end',
        flex: 1,
    },
    boxText2: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 10,
    },
});

export default App2
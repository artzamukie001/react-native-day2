import React from 'react';
import { StyleSheet, Text, View, Modal, TouchableOpacity } from 'react-native';

class ex4 extends React.Component {

    // state = {
    //     isShow: false,
    //     message: ""
    // }
    // onShowModal = (message) => {
    //     this.setState({ isShow: true, message })
    // }
    // onHideModal = () => {
    //     this.setState({ isShow: false, })
    // }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.text}>ผลิตภัณฑ์</Text>
                </View>

                <View style={styles.content}>
                    <View style={styles.row}>
                        <View style={styles.box}>
                            <TouchableOpacity>
                                <Text style={styles.text}
                                    // onPress={() => { this.onShowModal("Lorme1") }}
                                >Lorme1
                                </Text>
                            </TouchableOpacity>

                        </View>
                    </View>

                </View>




            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: 'red',
        flex: 1
    },
    header: {
        backgroundColor: 'blue',
        alignItems: 'center',
        flexDirection: 'column',
    },
    text: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 30,
    },
    content: {
        backgroundColor: 'green',
        flex: 1,
        flexDirection: 'column',
    },
    row: {
        backgroundColor: 'gray',
        flex: 1,
        flexDirection: 'row',
    },
    box: {
        backgroundColor: '#66FF00',
        flex: 1,
        margin: 14,
        flexDirection: 'column',
        alignItems: 'center',

    });

export default ex4
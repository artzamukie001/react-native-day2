import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Alert, TextInput, Button } from 'react-native';

class App extends React.Component {

    state = {
        username: 'username',
        password: 'password'
    }
    onShowAlert = () => {
        if (this.state.username === 'admin') {
            if (this.state.password === 'eiei') {
                return Alert.alert('Yahoo!!!')
            }
        } else return Alert.alert('Username or Password wrong !!!! ')
    }


    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.row}>
                        <View style={styles.column}>
                            <View style={styles.image}>
                                <Image
                                    style={styles.image}
                                    source={{ uri: 'https://scontent-kut2-2.xx.fbcdn.net/v/t1.0-9/38011840_2447134912016263_7498373180617129984_n.jpg?_nc_cat=111&_nc_ht=scontent-kut2-2.xx&oh=514f87320fefb8b7cfdeaf20499cc65f&oe=5CF1A91A' }}
                                />
                            </View>
                        </View>
                    </View>
                </View>

                <View style={styles.content}>
                    <View style={styles.column}>
                        <View style={styles.inputtex}>
                            <TextInput style={styles.text}
                                onChangeText={(value) => this.setState({ username: value })}
                                placeholder='username'
                            />
                        </View>
                        <View style={styles.inputtex}>
                            <TextInput style={styles.text}
                                onChangeText={(password) => this.setState({ password })}
                                placeholder='password'
                            />
                        </View>
                    </View>

                    <View style={styles.column}>
                        <View style={styles.center}>
                            <TouchableOpacity>
                                <Button title="Login" onPress={() => { this.onShowAlert() }} />
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>


            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'red',
        flex: 1
    },
    header: {
        backgroundColor: 'blue',
        flex: 1
    },
    content: {
        backgroundColor: 'yellow',
        flex: 1
    },
    row: {
        backgroundColor: 'gray',
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
    },
    column: {
        backgroundColor: 'gray',
        flexDirection: 'column',
        alignItems: 'center',
        flex: 1,
    },
    image: {
        backgroundColor: 'white',
        width: 200,
        height: 200,
        borderRadius: 100,
    },

    inputtex: {
        backgroundColor: 'white',
        flexDirection: 'column',
        alignItems: 'center',
        width: 300,
        height: 60,
        margin: 14,
    },
    text: {
        fontSize: 20,
        fontWeight: 'bold',
        padding: 10,
    },
    center: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },


})
export default App
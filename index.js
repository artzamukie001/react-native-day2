/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
import ex4 from './ex4';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => ex4);
